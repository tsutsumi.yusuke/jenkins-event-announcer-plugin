package org.jenkinsci.plugins.eventannouncer;

import hudson.model.FreeStyleBuild;
import hudson.model.FreeStyleProject;
import hudson.model.listeners.RunListener;
import hudson.tasks.Shell;
import jenkins.model.Jenkins;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.jvnet.hudson.test.JenkinsRule;
import org.jvnet.hudson.test.recipes.WithPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by tsutsumi on 1/2/15.
 */
public class TestListenerRun {

    @Rule
    public JenkinsRule j = new JenkinsRule();
    private FreeStyleBuild build;

    @Test
    public void testGenerateEvent() throws Exception {
        FreeStyleProject project = j.createFreeStyleProject();
        project.getBuildersList().add(new Shell("echo hello"));
        build = project.scheduleBuild2(0).get();
        build.run();
        Event event = ListenerRun.generateEvent(EventType.JOB_COMPLETED, build);
        Map<String, Object> attributes = event.attributes;
        assert attributes.get("run") == build;
        assert attributes.get("job") == build.getParent();
        Map<String, Object> m = (Map<String, Object>) attributes.get("scm");
        assert m.size() == 2;
    }
}
