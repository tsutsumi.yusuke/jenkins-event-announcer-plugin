package org.jenkinsci.plugins.eventannouncer;

import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.mock.PartialMock;
import org.unitils.mock.core.PartialMockObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tsutsumi on 1/2/15.
 */
public class TestAnnouncerHTTP {

    private Mustache.Compiler mustacheCompiler;
    private Map<String, Object> eventAttributes;
    private Event event;
    private PartialMock<AnnouncerHTTP> announcer;

    @Before
    public void setUp() {
        mustacheCompiler = AnnouncerHTTP.getMustacheCompiler();
        eventAttributes = new HashMap<String, Object>();
        event = new Event(EventType.JOB_STARTED, eventAttributes);
        announcer = new PartialMockObject<AnnouncerHTTP>(AnnouncerHTTP.class, this);
    }

    @Test
    public void testRenderURL() throws Exception {
        String templateString = "foo://{{eventType}}";
        Template template = mustacheCompiler.compile(templateString);
        Assert.assertEquals("foo://job_started", AnnouncerHTTP.renderURL(template, event));
    }

    @Test
    public void testAnnounce() {
        announcer.stub().getRequest(null);
        announcer.returns("foo://{{eventType}}").getUrlTemplate();
        announcer.getMock().mustacheCompiler = AnnouncerHTTP.getMustacheCompiler();
        announcer.getMock().announce(event);
        announcer.assertInvoked().getRequest("foo://job_started");
    }
}
