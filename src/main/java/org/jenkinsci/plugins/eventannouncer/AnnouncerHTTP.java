package org.jenkinsci.plugins.eventannouncer;

import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.MustacheException;
import com.samskivert.mustache.Template;
import hudson.Extension;
import hudson.model.Descriptor;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.DataBoundSetter;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by tsutsumi on 11/9/14.
 */
public class AnnouncerHTTP extends Announcer {

    Mustache.Compiler mustacheCompiler = getMustacheCompiler();
    protected static final HttpClient httpClient = getHttpClient();
    protected static final Logger LOG = Logger.getLogger(AnnouncerHTTP.class.getName());

    protected Template _compiledTemplate;
    protected String _urlTemplate;

    public static HttpClient getHttpClient() {
        PoolingClientConnectionManager conman = new PoolingClientConnectionManager();
        conman.setMaxTotal(20);
        conman.setDefaultMaxPerRoute(20);
        return new DefaultHttpClient(conman);
    }

    @DataBoundConstructor
    public AnnouncerHTTP(EnumSet<EventType> eventTypes,  String urlTemplate) {
        this._eventTypes = eventTypes;
        this._urlTemplate = urlTemplate;
    }

    public String getUrlTemplate() { return _urlTemplate; }

    @DataBoundSetter
    public void setUrlTemplate(String ut) {
        this._urlTemplate = ut;
        _compiledTemplate = null;
    }

    @Override
    public void announce(Event event) {
        LOG.info("Announcing event.");
        String url = renderURL(getCompiledTemplate(), event);
        this.getRequest(url);
    }

    protected static Mustache.Compiler getMustacheCompiler() {
        return Mustache.compiler().withFormatter(new MustacheURLEscapeFormatter());
    }

    protected static String renderURL(Template compiledTemplate, Event event) {
        Map<String, Object> parameters = new HashMap<String, Object>(event.attributes);
        parameters.put("eventType", event.eventType.name);

        return compiledTemplate.execute(parameters);
    }

    protected Template getCompiledTemplate() {
        if (_compiledTemplate == null) {
            _compiledTemplate = mustacheCompiler.compile(getUrlTemplate());
        }
        return _compiledTemplate;
    }

    @Extension
    public static class DescriptorImpl extends Descriptor<Announcer> {
        @Override
        public String getDisplayName() {
            return "HTTP Announcer";
        }
    }

    public static class MustacheURLEscapeFormatter implements Mustache.Formatter {

        public String format(Object o) {
            try {
                return URLEncoder.encode(o.toString(), "UTF-8");
            } catch (IOException e) {
                throw new MustacheException(e);
            }
        }
    }

    public boolean getRequest(String url) {
        // HTTP GET request
        try {

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            // optional default is GET
            con.setRequestMethod("GET");

            int responseCode = con.getResponseCode();
            LOG.info("\nSending 'GET' request to URL : " + url);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return true;

        } catch (Exception e) {
            LOG.throwing(this.getClass().getName(), "Could not request " + url, e);
            return false;
        }

    }

}
