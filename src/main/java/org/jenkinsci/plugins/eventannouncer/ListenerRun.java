package org.jenkinsci.plugins.eventannouncer;

import hudson.Extension;
import hudson.model.AbstractBuild;
import hudson.model.Run;
import hudson.model.TaskListener;
import hudson.model.listeners.RunListener;
import hudson.scm.ChangeLogSet;
import hudson.triggers.SCMTrigger;

import java.util.*;
import java.util.logging.Logger;

/**
 * Created by tsutsumi on 11/9/14.
 */
@Extension
public class ListenerRun extends RunListener<Run> {

    protected final static Logger LOG = Logger.getLogger(ListenerRun.class.getName());

    public static Event generateEvent(EventType type, Run r) {
        Map attributes = new HashMap<String, Object>();
        attributes.put("run", r);
        attributes.put("job", r.getParent());
        attributes.put("scm", parseSCMInformation(r));

        return new Event(type, attributes);
    }

    public static HashMap<String, Object> parseSCMInformation(Run r) {
        HashMap<String, Object> scmMap = new HashMap<String, Object>();
        if (r instanceof AbstractBuild) {
            AbstractBuild build = (AbstractBuild) r;
            scmMap.put("changeSet", build.getChangeSet());
            // build up a short description of each change set, for convenience
            List<String> commitDescriptions = new ArrayList<String>();
            Set<String> authors = new HashSet<String>();
            for (Object rawEntry : build.getChangeSet().getItems()) {
                if (rawEntry instanceof ChangeLogSet.Entry) {
                    ChangeLogSet.Entry entry = (ChangeLogSet.Entry) rawEntry;
                    commitDescriptions.add(buildSCMString(entry));
                    authors.add(entry.getAuthor().getFullName());
                } else {
                    LOG.warning("Unable to cast changelogset element to entry for: " + rawEntry.toString());
                }
            }
            scmMap.put("commitDescriptions", commitDescriptions);
            scmMap.put("authors", authors);

        } else {
            LOG.warning("Unable to cast run to AbstractBuild for: " + r.getParent().getName());
        }
        return scmMap;
    }

    public static String buildSCMString(ChangeLogSet.Entry scmEntry) {
        return "" + scmEntry.getAuthor().getDisplayName() +
                ": " + scmEntry.getMsg() +
                " (" + scmEntry.getCommitId() + ")";
    }

    @Override
    public void onStarted(Run r, TaskListener listener) {
        EventAnnouncerPlugin.announceAll(generateEvent(EventType.JOB_STARTED, r));
    }

    @Override
    public void onCompleted(Run r, TaskListener listener) {
        EventAnnouncerPlugin.announceAll(generateEvent(EventType.JOB_COMPLETED, r));
    }
}
