package org.jenkinsci.plugins.eventannouncer;

import hudson.Extension;
import hudson.Launcher;
import hudson.model.*;
import hudson.tasks.*;
import hudson.util.DescribableList;
import hudson.util.ListBoxModel;
import jenkins.model.Jenkins;
import net.sf.json.JSONObject;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.DataBoundSetter;
import org.kohsuke.stapler.StaplerRequest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by tsutsumi on 12/30/14.
 */
public class IndividualBuildAnnouncer extends Notifier {

    private Announcer announcer;

    @DataBoundConstructor
    public IndividualBuildAnnouncer(Announcer announcer) {
        this.announcer = announcer;
    }

    public Announcer getAnnouncer() {
        return announcer;
    }

    @DataBoundSetter
    public void setAnnouncer(Announcer announcer) {
        this.announcer = announcer;
    }

    @Override
    public boolean perform(AbstractBuild<?, ?> build, final Launcher launcher, final BuildListener listener)
            throws InterruptedException, IOException {
        Event event = ListenerRun.generateEvent(EventType.JOB_COMPLETED, build);
        announcer.announce(event);
        return true;
    }

    @Override
    public BuildStepMonitor getRequiredMonitorService() {
        return BuildStepMonitor.NONE;
    }

    public static class DescriptorImpl extends BuildStepDescriptor<Publisher> {
        @Override
        public boolean isApplicable(Class<? extends AbstractProject> jobType) {
            return true;
        }

        @Override
        public String getDisplayName() {
            return "Announce a build is complete";
        }

        public List<Descriptor<Announcer>> getAnnouncerDescriptors() {
            return Jenkins.getInstance().getDescriptorList(Announcer.class);
        }
    }
}
