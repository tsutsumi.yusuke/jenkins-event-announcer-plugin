package org.jenkinsci.plugins.eventannouncer;

import hudson.model.AbstractDescribableImpl;
import hudson.model.Descriptor;

import java.util.*;

/**
 * Created by tsutsumi on 11/9/14.
 */
public abstract class Announcer extends AbstractDescribableImpl<Announcer> {

    public abstract void announce(Event event);
    protected EnumSet<EventType> _eventTypes;

    // @DataBoundSetter only works with Jenkins > 1.535
    public void setEventTypes(EnumSet<EventType> set) {
        _eventTypes = set;
    }

    public EnumSet<EventType> getEventTypes() {
        return _eventTypes;
    }

    public void announceAndFilter(Event e) {
        if (_eventTypes.contains(e.eventType)) {
            announce(e);
        }
    }
}
