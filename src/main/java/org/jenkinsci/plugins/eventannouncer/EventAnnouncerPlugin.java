package org.jenkinsci.plugins.eventannouncer;

import hudson.Plugin;
import hudson.model.Descriptor;
import hudson.util.DescribableList;
import jenkins.model.Jenkins;
import net.sf.json.JSONObject;
import org.kohsuke.stapler.StaplerRequest;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by tsutsumi on 11/9/14.
 */
public class EventAnnouncerPlugin extends Plugin {

    public final DescribableList<Announcer, Descriptor<Announcer>> announcers =
            new DescribableList<Announcer, Descriptor<Announcer>>(this);

    public void start() throws IOException {
        load();
    }

    public static void announceAll(Event e) {
        EventAnnouncerPlugin plugin = Jenkins.getInstance().getPlugin(EventAnnouncerPlugin.class);
        if (plugin != null ) {
            for (Announcer announcer : plugin.getAnnouncers()) {
                announcer.announceAndFilter(e);
            }
        }
    }

    public final boolean showEventTypes = true;
    public boolean getShowEventTypes() { return true; }

    @Override
    public void configure(StaplerRequest req, JSONObject formData) throws IOException, Descriptor.FormException {
        try {
            announcers.rebuildHetero(req, formData, getAnnouncerDescriptors(), "announcers");
            save();
        } catch (IOException e) {
            throw new Descriptor.FormException(e, "announcers");
        }
    }

    public List<Announcer> getAnnouncers() { return announcers.toList(); }

    public void setAnnouncers(List<Announcer> newAnnouncers) {
        announcers.clear();
        for (Announcer announcer : newAnnouncers) {
            announcers.add(announcer);
        }
    }

    public List<Descriptor<Announcer>> getAnnouncerDescriptors() {
        return Jenkins.getInstance().getDescriptorList(Announcer.class);
    }
}
