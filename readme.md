# Jenkins Event Announcer Plugin

The Jenkins Event Announcer Plugin allows jenkins to announce the occurence of events
with various metadata to a variety of listeners. Currently it only supports http.

Currently, the following events are hooked:

* Job creation and deletion.

![plugin example](https://raw.githubusercontent.com/toumorokoshi/jenkins-event-announcer-plugin/master/etc/example.png)
